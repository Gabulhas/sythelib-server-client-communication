from GameEntity.GameObject.GetGameObject import GetGameObject
from GameEntity.NPC.GetNPC import GetNPC
from LocalPlayer.getPlayer import getPlayer
from GameEntity.Player.GetPlayer import GetPlayer


class ClientConnection:
    def __init__(self, ip, port):
        self.__connection_string = "http://{}:{}".format(ip, port)
        self.NPCS = GetNPC(self)
        self.LocalPlayer = getPlayer(self)
        self.GameObjects = GetGameObject(self)
        self.Players = GetPlayer(self)

    def getConnectionString(self):
        return self.__connection_string
