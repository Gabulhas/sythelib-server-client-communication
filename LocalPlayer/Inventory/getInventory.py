import requests

from other.item import ItemObject


class getInventory:
    def __init__(self, player):
        self.__player = player

    @staticmethod
    def __items_from_json__(data):
        items_json = data["items"]
        allItems = []
        for item in items_json:
            if item["name"] == "null":
                continue;
            allItems.append(ItemObject(item["id"], item["amount"], item["name"], item["slot"]))
        return allItems

    def get_all_items(self):
        r = requests.get(self.__player.cc.getConnectionString() + "/Inventory")
        data = r.json()
        return self.__items_from_json__(data)

    def get_item_at_slot(self, slot):
        return self.get_all_items()[slot]

    def get_item_by_name(self, name="", id=0):

        allItems = []
        for item in self.get_all_items():
            if item.name == name or item.id == id:
                allItems.append(item)
        return allItems

    def get_empty_slots(self):
        allItems = self.get_all_items()
        return 28 - len(allItems)

    def is_full(self):
        if self.get_empty_slots() == 0:
            return True
        return False

    def is_empty(self):
        if self.get_empty_slots() == 28:
            return True
        return False

    def contains(self, name="", id_=-1):
        all_items = self.get_all_items()

        for item in all_items:
            if item.name == name or item.id == id_:
                return True

        return False
