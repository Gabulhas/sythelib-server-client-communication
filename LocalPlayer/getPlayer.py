import requests

from LocalPlayer.Inventory.getInventory import getInventory
from LocalPlayer.Stats.getSkill import getSkill
from other.equipment.getEquipment import getEquipment
from other.position import Postition


class getPlayer:
    def __init__(self, cc):
        self.cc = cc
        self.inventory = getInventory(self)
        self.stats = getSkill(self)
        self.equipment = getEquipment(self, True, {})

    def get_animation(self):
        r = requests.get(self.cc.getConnectionString() + "/Animation")
        data = r.json()
        return data["animation"]

    def is_idle(self):
        if self.get_animation() == -1:
            return True
        return False

    def get_spot_animation(self):
        r = requests.get(self.cc.getConnectionString() + "/Animation")
        data = r.json()
        return data["spotAnimation"]

    def get_pose_animation(self):
        r = requests.get(self.cc.getConnectionString() + "/Animation")
        data = r.json()
        return data["pose"]

    def get_pose_idle_animation(self):
        r = requests.get(self.cc.getConnectionString() + "/Animation")
        data = r.json()
        return data["poseIdle"]

    def is_standing_still(self):
        if self.get_pose_idle_animation() == self.get_pose_animation():
            return True
        return False

    def get_pos(self):
        r = requests.get(self.cc.getConnectionString() + "/Pos")
        data = r.json()
        return Postition(data["x"], data["y"])

    def get_hp(self):
        return self.stats.Hitpoints().boosted
