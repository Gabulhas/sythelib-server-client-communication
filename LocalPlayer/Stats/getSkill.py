import requests

from LocalPlayer.Stats.skill import skill


class getSkill:
    def __init__(self, player):
        self.__player = player

    @staticmethod
    def __stats_from_json__(data):
        all_stats = {}
        for st in data:
            tempSkill = data[st]
            all_stats[st] = (skill(tempSkill["level"], tempSkill["boostedLevel"], tempSkill["xp"]))
        return all_stats

    def __get_single_stat__(self,name):
        r = requests.get(self.__player.cc.getConnectionString() + "/skills")
        data = r.json()
        return getSkill.__stats_from_json__(data)[name]

    def All(self):
        r = requests.get(self.__player.cc.getConnectionString() + "/skills")
        data = r.json()

        return self.__stats_from_json__(data)

    def Attack(self):
        return self.__get_single_stat__("Attack")

    def Defence(self):
        return self.__get_single_stat__("Defence")

    def Strength(self):
        return self.__get_single_stat__("Strength")

    def Hitpoints(self):
        return self.__get_single_stat__("Hitpoints")

    def Ranged(self):
        return self.__get_single_stat__("Ranged")

    def Prayer(self):
        return self.__get_single_stat__("Prayer")

    def Magic(self):
        return self.__get_single_stat__("Magic")

    def Cooking(self):
        return self.__get_single_stat__("Cooking")

    def Woodcutting(self):
        return self.__get_single_stat__("Woodcutting")

    def Fletching(self):
        return self.__get_single_stat__("Fletching")

    def Fishing(self):
        return self.__get_single_stat__("Fishing")

    def Firemaking(self):
        return self.__get_single_stat__("Firemaking")

    def Crafting(self):
        return self.__get_single_stat__("Crafting")

    def Smithing(self):
        return self.__get_single_stat__("Smithing")

    def Mining(self):
        return self.__get_single_stat__("Mining")

    def Herblore(self):
        return self.__get_single_stat__("Herblore")

    def Agility(self):
        return self.__get_single_stat__("Agility")

    def Thieving(self):
        return self.__get_single_stat__("Thieving")

    def Slayer(self):
        return self.__get_single_stat__("Slayer")

    def Farming(self):
        return self.__get_single_stat__("Farming")

    def Runecraft(self):
        return self.__get_single_stat__("Runecraft")

    def Hunter(self):
        return self.__get_single_stat__("Hunter")

    def Construction(self):
        return self.__get_single_stat__("Construction")
