import math

import requests

from GameEntity.Player.Player import Player
from other.clickbox import Clickbox
from other.position import Postition


class GetPlayer:
    def __init__(self, cc):
        self.cc = cc

    @staticmethod
    def __players_from_json__(jsonObject):
        fetchedPlayers = jsonObject
        allNpcs = []
        for player in fetchedPlayers:
            if "name" not in player:
                continue
            player_position = Postition(player["pos"]["x"], player["pos"]["y"])
            if "canvas" in player:
                player_clickbox = player["canvas"]["polys"][0]["verts"]
                player_clickbox_obj = Clickbox(player_clickbox)
            else:
                player_clickbox_obj = Clickbox([])
            allNpcs.append(
                Player(player["localPlayer"], player["id"], player["name"], player["level"], player["healthRatio"],
                       player["healthScale"], player["interacting"], player["interactingID"], player_position,
                       player_clickbox_obj, player["skulled"], player["overhead"], player["equipment"]))

        return allNpcs

    def get_players(self, name=""):
        r = requests.get(self.cc.getConnectionString() + "/players")
        data = r.json()
        allplayers = self.__players_from_json__(data)
        if name == "":
            return allplayers
        else:
            new_allplayers = []
            for player in allplayers:
                if player.name == name:
                    new_allplayers.append(player)
            return new_allplayers

    def get_local_player(self):
        all_players = self.get_players()
        for player in all_players:
            if player.localplayer is True:
                return player

    def get_interacting_with_local_player(self):
        all_players = self.get_players()
        local_player_name = self.get_local_player().name
        for player in all_players:
            if player.interacting == local_player_name:
                return player

    def get_nearest_player(self, name=""):
        allplayers = self.get_players(name)

        playerX, playerY = self.cc.LocalPlayer.get_pos().get_position()

        minimal_distance = 50000000000
        playerTemp = allplayers[0]

        for player in allplayers:
            playerX, playerY = player.pos.get_position()

            distance = math.hypot(playerX - playerX, playerY - playerY)
            if distance < minimal_distance:
                minimal_distance = distance
                playerTemp = player

        return playerTemp
