from GameEntity.GameEntity import GameEntity
from other.equipment.getEquipment import getEquipment


class Player(GameEntity):
    def __init__(self, localplayer, id, name, combatLevel, health_ratio, health_scale, interacting, interacting_id, pos,
                 clickbox,
                 skulled, overhead, equipment):
        super().__init__(id, name, pos, clickbox)

        self.localplayer = localplayer
        self.combatLevel = combatLevel
        self.health_ratio = health_ratio
        self.health_scale = health_scale
        self.interacting = interacting
        self.clickbox = clickbox
        self.interacting = interacting
        self.interacting_id = interacting_id
        self.overhead = overhead
        self.skulled = skulled
        self.equipment = getEquipment(self, False, equipment)

