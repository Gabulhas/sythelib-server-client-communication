from GameEntity.GameEntity import GameEntity
from other.camera import rotate_camera_180


class GameObject(GameEntity):
    def __init__(self, entity_id, name, pos, clickbox):
        super().__init__(entity_id, name, pos, clickbox)

    def get_pos(self):
        return self.pos.x, self.pos.y
