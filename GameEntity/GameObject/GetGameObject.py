import math

import requests

from GameEntity.GameObject.GameObject import GameObject
from other.clickbox import Clickbox
from other.position import Postition


class GetGameObject:
    def __init__(self, cc):
        self.cc = cc

    @staticmethod
    def __game_objects_from_json__(data):
        allObjects = []
        for gameObj in data:
            object_pos = Postition(gameObj["pos"]["x"], gameObj["pos"]["y"])
            if "canvas" in gameObj:
                object_clickbox = gameObj["canvas"]["polys"][0]["verts"]
                object_clickbox_obj = Clickbox(object_clickbox)
            else:
                object_clickbox_obj = Clickbox([])
            allObjects.append(GameObject(gameObj["id"], gameObj["name"], object_pos, object_clickbox_obj))

        return allObjects

    def all(self, name="", id_=-1):
        r = requests.get(self.cc.getConnectionString() + "/gameobjects")
        data = r.json()
        allObjs = self.__game_objects_from_json__(data)
        if name == "" and id_ == -1:
            return allObjs
        else:
            new_allObjs = []
            for npc in allObjs:
                if npc.name == name or npc.id == id_:
                    new_allObjs.append(npc)
            return new_allObjs

    def multiple_ids(self, ids):

        allObjs = self.all()
        newAllObjs = []

        for npc in allObjs:
            if npc.id in ids:
                newAllObjs.append(npc)
        return newAllObjs

    def multiple_names(self, names):

        allObjs = self.all()
        newAllObjs = []

        for npc in allObjs:
            if npc.name in names:
                newAllObjs.append(npc)
        return newAllObjs

    # to Player
    def nearest_obj(self, name="", id_=-1) -> GameObject:
        allObjs = self.all(name, id_)
        playerX, playerY = self.cc.LocalPlayer.get_pos().get_position()
        return self.__get_nearest_to_point__(playerX, playerY, allObjs)

    def nearest_obj_names(self, names):
        allObjs = self.multiple_names(names)
        playerX, playerY = self.cc.LocalPlayer.get_pos().get_position()
        return self.__get_nearest_to_point__(playerX, playerY, allObjs)

    def nearest_obj_ids(self, ids):
        allObjs = self.multiple_ids(ids)
        playerX, playerY = self.cc.LocalPlayer.get_pos().get_position()
        return self.__get_nearest_to_point__(playerX, playerY, allObjs)

    # to Point
    def nearest_to_point(self, x, y, name="", id_=-1):
        allObjs = self.all(name, id_)
        return self.__get_nearest_to_point__(x, y, allObjs)

    def nearest_to_point_names(self, x, y, names):
        allObjs = self.multiple_names(names)
        return self.__get_nearest_to_point__(x, y, allObjs)

    def nearest_to_point_ids(self, x, y, ids):
        allObjs = self.multiple_ids(ids)
        return self.__get_nearest_to_point__(x, y, allObjs)

    @staticmethod
    def __get_nearest_to_point__(x, y, allObjs):

        if not allObjs:
            return None
        pointX = x
        pointY = y

        minimal_distance = 50000000000
        objTemp = allObjs[0]

        for obj in allObjs:
            objX, objY = obj.pos.get_position()

            distance = math.hypot(pointX - objX, pointY - objY)
            if distance < minimal_distance:
                minimal_distance = distance
                objTemp = obj

        return objTemp
