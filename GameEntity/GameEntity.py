from other.clickbox import Clickbox


class GameEntity:
    def __init__(self, id, name, pos, clickbox):
        self.id = id
        self.name = name
        self.pos = pos
        self.__clickbox__ = clickbox

    def clicks(self) -> Clickbox:
        return self.__clickbox__
