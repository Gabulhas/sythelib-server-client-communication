import math

import requests

from other.clickbox import Clickbox
from other.position import Postition

from GameEntity.NPC.NPC import NPC


class GetNPC:
    def __init__(self, cc):
        self.cc = cc

    @staticmethod
    def __npcs_from_json__(jsonObject):
        fetchedNPCs = jsonObject
        allNpcs = []
        for npc in fetchedNPCs:
            if "name" not in npc:
                continue
            npc_position = Postition(npc["pos"]["x"], npc["pos"]["y"])
            if "canvas" in npc:
                npc_clickbox = npc["canvas"]["polys"][0]["verts"]
                npc_clickbox_obj = Clickbox(npc_clickbox)
            else:
                npc_clickbox_obj = Clickbox([])
            allNpcs.append(
                NPC(npc["name"], npc["combatLvl"], npc["healthRatio"], npc["healthScale"], npc["interacting"],
                    npc_position, npc_clickbox_obj, npc["index"]))

        return allNpcs

    def get_npcs(self, name=""):
        r = requests.get(self.cc.getConnectionString() + "/Npcs")
        data = r.json()
        allNpcs = self.__npcs_from_json__(data)
        if name == "":
            return allNpcs
        else:
            new_allNpcs = []
            for npc in allNpcs:
                if npc.name == name:
                    new_allNpcs.append(npc)
            return new_allNpcs

    def get_nearest_npc(self, name=""):
        allNpcs = self.get_npcs(name)

        playerX, playerY = self.cc.LocalPlayer.get_pos().get_position()

        minimal_distance = 50000000000
        npcTemp = allNpcs[0]

        for npc in allNpcs:
            npcX, npcY = npc.pos.get_position()

            distance = math.hypot(playerX - npcX, playerY - npcY)
            if distance < minimal_distance:
                minimal_distance = distance
                npcTemp = npc

        return npcTemp
