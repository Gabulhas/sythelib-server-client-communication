from GameEntity.GameEntity import GameEntity


class NPC(GameEntity):
    def __init__(self, name, combatLevel, health_ratio, health_scale, interacting, pos, clickbox, index):
        super().__init__(index, name, pos, clickbox)
        self.combatLevel = combatLevel
        self.health_ratio = health_ratio
        self.health_scale = health_scale
        self.interacting = interacting
