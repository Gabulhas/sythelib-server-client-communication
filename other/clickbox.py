import random
import math
from shapely.geometry import Polygon, Point
from pylibsythe import *
import sythelib_utils as utils


class Clickbox:
    def __init__(self, verts):
        self.verts = verts

    def get_canvas(self):
        return self.verts

    def click(self):
        p = self.__get_random_point_in_polygon(Polygon(self.verts))
        utils.click_random_circle_point(p.x, p.y, 1, 0)

    def click_center(self):
        centroid = Polygon(self.verts).centroid
        utils.click_random_circle_point(int(centroid.x), int(centroid.y), 1, 0)

    def click_instant_center(self):
        centroid = Polygon(self.verts).centroid
        click_mouse(int(math.floor(centroid.x)), int(math.floor(centroid.y)), 1)

    def click_instant_random(self):
        p = self.__get_random_point_in_polygon(Polygon(self.verts))
        click_mouse(math.ceil(p.x), math.ceil(p.y), 1)

    def move(self):
        p = self.__get_random_point_in_polygon(Polygon(self.verts))
        utils.random_move_mouse(int(p.x), int(p.y))

    def move_center(self):
        centroid = Polygon(self.verts).centroid
        utils.random_move_mouse(int(math.floor(centroid.x)), int(math.floor(centroid.y)))

    def move_instant_center(self):
        centroid = Polygon(self.verts).centroid
        move_mouse(int(math.floor(centroid.x)), int(math.floor(centroid.y)))

    def move_instant_random(self):
        p = self.__get_random_point_in_polygon(Polygon(self.verts))
        move_mouse(int(math.floor(p.x)), int(math.floor(p.y)))

    @staticmethod
    def __get_random_point_in_polygon(polygon: Polygon):
        minx, miny, maxx, maxy = polygon.bounds
        while True:
            pnt = Point(random.uniform(minx, maxx), random.uniform(miny, maxy))
            if polygon.contains(pnt):
                return pnt
