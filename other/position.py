class Postition:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return self.get_position()

    def get_position(self):
        return self.x, self.y
