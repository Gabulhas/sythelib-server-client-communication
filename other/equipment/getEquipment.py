import requests

from other.item import ItemObject


class getEquipment:
    def __init__(self, player, is_local_player, equips):
        self.all_equips = []
        self.__player = player
        self.is_local_player = is_local_player
        if not is_local_player:
            self.all_equips = self.__items_from_json__(equips)

    @staticmethod
    def __items_from_json__(data):
        allItems = {}

        for slot in data:
            tempSlot = data[slot]
            allItems[slot] = ItemObject(tempSlot["id"], tempSlot["amount"], tempSlot["name"], tempSlot["slot"])
        return allItems

    def __get_single_slot__(self, name):
        if self.is_local_player:
            r = requests.get(self.__player.cc.getConnectionString() + "/equipment")
            data = r.json()
            self.all_equips = self.__items_from_json__(data)
        return self.all_equips[name]

    def All(self):
        if self.is_local_player:
            r = requests.get(self.__player.cc.getConnectionString() + "/equipment")
            data = r.json()
            self.all_equips = self.__items_from_json__(data)
        return self.all_equips

    def head(self):
        return self.__get_single_slot__("head")

    def cape(self):
        return self.__get_single_slot__("cape")

    def amulet(self):
        return self.__get_single_slot__("amulet")

    def weapon(self):
        return self.__get_single_slot__("weapon")

    def torso(self):
        return self.__get_single_slot__("torso")

    def shield(self):
        return self.__get_single_slot__("shield")

    def legs(self):
        return self.__get_single_slot__("legs")

    def gloves(self):
        return self.__get_single_slot__("gloves")

    def boots(self):
        return self.__get_single_slot__("boots")

    def ring(self):
        return self.__get_single_slot__("ring")

    def ammo(self):
        return self.__get_single_slot__("ammo")
